package tests

import (
	"gotest/common/models"
	"testing"
)

func TestBookModel(t *testing.T) {
	book := *models.NewBook("123-456-789", "Libro Uno", "Jhon Doe", "Literatura")

	if book.ISBN != "123-456-789" {
		t.Errorf("book ISBN not or bad assignment!")
	}

	if book.Title != "Libro Uno" {
		t.Errorf("book Title not or bad assignment!")
	}

	if book.Author != "Jhon Doe" {
		t.Errorf("book Author not or bad assignment!")
	}

	if book.Category != "Literatura" {
		t.Errorf("book Category not or bad assignment!")
	}
}
