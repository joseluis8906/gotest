package usecases

import (
	"gotest/common/contracts"
	"gotest/common/models"
)

// BooksUseCase is a struct to handle books actions
type BooksUseCase struct {
	Repository contracts.BooksRepositoryContract
}

// NewBooksUseCase is a BooksUseCase constructor
func NewBooksUseCase(Repository contracts.BooksRepositoryContract) *BooksUseCase {
	return &BooksUseCase{
		Repository: Repository,
	}
}

// Create method of BooksUseCase to envolve business logic of book creation
func (UseCase BooksUseCase) Create(isbn string, title string, author string, category string) error {
	err := UseCase.Repository.Create(isbn, title, author, category)
	if err != nil {
		return err
	}
	return nil
}

// GetOne method of BooksUseCase to envolve business logic to find one book
func (UseCase BooksUseCase) GetOne(isbn string) (*models.Book, error) {
	book, err := UseCase.Repository.FindOne(isbn)
	if err != nil {
		return nil, err
	}
	return book, nil
}

// GetByCategory method of BooksUseCase to envolve bussines logic to find all books by category
func (UseCase BooksUseCase) GetByCategory(category string) (*[]models.Book, error) {
	books, err := UseCase.Repository.FindByCategory(category)
	if err != nil {
		return nil, err
	}
	return books, nil
}

// EraseOne method of BooksUseCase to envolve bussines logic to delete one book by isbn
func (UseCase BooksUseCase) EraseOne(isbn string) error {
	err := UseCase.Repository.DeleteOne(isbn)
	if err != nil {
		return err
	}
	return nil
}
