package contracts

import (
	"context"

	"github.com/aws/aws-lambda-go/events"
)

// AwsContract is a mandatory interface to handle aws request and response
type AwsContract interface {
	Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error)
}
