package contracts

import (
	"gotest/common/models"
)

// BooksRepositoryContract is a contract will be implemented for concrete book repository
type BooksRepositoryContract interface {
	Create(isbn string, title string, author string, category string) error
	FindOne(isbn string) (*models.Book, error)
	FindByCategory(category string) (*[]models.Book, error)
	DeleteOne(isbn string) error
}
