package repositories

import (
	"gotest/common/models"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// BooksDynamoRepository is a struct to manage books models
type BooksDynamoRepository struct {
	Client *dynamodb.DynamoDB
	Model  string
}

// NewBooksDynamoRepository is BooksDynamoRepository constructor
func NewBooksDynamoRepository(Client *dynamodb.DynamoDB) *BooksDynamoRepository {
	return &BooksDynamoRepository{
		Client: Client,
		Model:  "Books",
	}
}

// Create is a method of BookRepositoryDynamoDB to create new book into data base
func (Repository BooksDynamoRepository) Create(isbn string, title string, author string, category string) error {
	input := &dynamodb.PutItemInput{
		TableName: aws.String(Repository.Model),
		Item: map[string]*dynamodb.AttributeValue{
			"ISBN": {
				S: aws.String(isbn),
			},
			"Title": {
				S: aws.String(title),
			},
			"Author": {
				S: aws.String(author),
			},
			"Category": {
				S: aws.String(category),
			},
		},
	}

	_, err := Repository.Client.PutItem(input)
	if err != nil {
		return err
	}

	return nil
}

// FindOne is a method of BookRepositoryDynamoDB to find a book in the data base
func (Repository BooksDynamoRepository) FindOne(isbn string) (*models.Book, error) {
	input := &dynamodb.GetItemInput{
		TableName: aws.String(Repository.Model),
		Key: map[string]*dynamodb.AttributeValue{
			"ISBN": {
				S: aws.String(isbn),
			},
		},
	}

	result, err := Repository.Client.GetItem(input)
	if err != nil {
		return nil, err
	}

	if result.Item == nil {
		return nil, nil
	}

	book := new(models.Book)
	err = dynamodbattribute.UnmarshalMap(result.Item, book)
	if err != nil {
		return nil, err
	}

	return book, nil
}

// FindByCategory is a method of BookRepositoryDynamoDB to find all books by category in the database
func (Repository BooksDynamoRepository) FindByCategory(category string) (*[]models.Book, error) {
	input := &dynamodb.QueryInput{
		TableName:              aws.String(Repository.Model),
		Select:                 aws.String("ALL_ATTRIBUTES"),
		IndexName:              aws.String("Category-index"),
		KeyConditionExpression: aws.String("Category = :category"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":category": {
				S: aws.String(category),
			},
		},
	}

	result, err := Repository.Client.Query(input)
	if err != nil {
		return nil, err
	}

	if result.Items == nil {
		return nil, nil
	}

	books := new([]models.Book)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, books)
	if err != nil {
		return nil, err
	}

	return books, nil
}

// DeleteOne is a method of BookRepositoryDynamoDB to delete a book from data base
func (Repository BooksDynamoRepository) DeleteOne(isbn string) error {
	input := &dynamodb.DeleteItemInput{
		TableName: aws.String(Repository.Model),
		Key: map[string]*dynamodb.AttributeValue{
			"ISBN": {
				S: aws.String(isbn),
			},
		},
	}

	_, err := Repository.Client.DeleteItem(input)
	if err != nil {
		return err
	}

	return nil
}
