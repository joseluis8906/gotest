package common

import (
	"gotest/common/contracts"

	"github.com/aws/aws-lambda-go/lambda"
)

// AwsLambda is a struct to implement mandatory methods of aws lambda functions
type AwsLambda struct {
	Controller contracts.AwsContract
}

// NewAwsLambda is a AwsLambda constructor
func NewAwsLambda(Controller contracts.AwsContract) *AwsLambda {
	return &AwsLambda{
		Controller: Controller,
	}
}

// Start is a prestart method to ensure controller implement AwsContract interface
func (Lambda AwsLambda) Start() {
	lambda.Start(Lambda.Controller.Handler)
}
