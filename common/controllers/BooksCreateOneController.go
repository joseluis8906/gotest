package controllers

import (
	"context"
	"encoding/json"
	"gotest/common/contracts"
	"gotest/common/models"

	"github.com/aws/aws-lambda-go/events"
)

// BooksCreateOneController is a struct to implement aws lambda request and response
type BooksCreateOneController struct {
	UseCase contracts.BooksUseCaseContract
}

// NewBooksCreateOneController is a NewBooksCreateOneController constructor
func NewBooksCreateOneController(UseCase contracts.BooksUseCaseContract) *BooksCreateOneController {
	return &BooksCreateOneController{
		UseCase: UseCase,
	}
}

// Handler is a method contract to implement a aws request and response handler
func (Controller *BooksCreateOneController) Handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	newBook := models.Book{}
	err := json.Unmarshal([]byte(request.Body), &newBook)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 200}, nil
	}

	err = Controller.UseCase.Create(newBook.ISBN, newBook.Title, newBook.Author, newBook.Category)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 200}, nil
	}

	body, err := json.Marshal(newBook)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Error: converting book to json", StatusCode: 200}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
}
