package controllers

import (
	"context"
	"gotest/common/contracts"

	"github.com/aws/aws-lambda-go/events"
)

// BooksDeleteOneController is a structu to handle aws request and response
type BooksDeleteOneController struct {
	UseCase contracts.BooksUseCaseContract
}

// NewBooksDeleteOneController is a constructor for BooksDeleteOneController
func NewBooksDeleteOneController(UseCase contracts.BooksUseCaseContract) *BooksDeleteOneController {
	return &BooksDeleteOneController{
		UseCase: UseCase,
	}
}

// Handler is a method contract to implement a aws request and response handler
func (Controller BooksDeleteOneController) Handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	err := Controller.UseCase.EraseOne(request.PathParameters["isbn"])
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 200}, nil
	}

	return events.APIGatewayProxyResponse{Body: "", StatusCode: 200}, nil
}
