package controllers

import (
	"context"
	"encoding/json"
	"gotest/common/contracts"

	"github.com/aws/aws-lambda-go/events"
)

// BooksFindByCategoryController is a struct to implement aws lambda request and response
type BooksFindByCategoryController struct {
	UseCase contracts.BooksUseCaseContract
}

// NewBooksFindByCategoryController is a BooksFindByCategoryController constructor
func NewBooksFindByCategoryController(UseCase contracts.BooksUseCaseContract) *BooksFindByCategoryController {
	return &BooksFindByCategoryController{
		UseCase: UseCase,
	}
}

// Handler is a method contract to implement a aws request and response handler
func (Controller BooksFindByCategoryController) Handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	books, err := Controller.UseCase.GetByCategory(request.QueryStringParameters["category"])
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 200}, nil
	}

	body, err := json.Marshal(books)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Error: converting books to json", StatusCode: 200}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
}
