.PHONY: build books-find-one clean deploy gomodgen

gomodgen:
	chmod u+x gomod.sh
	./gomod.sh

build: gomodgen
	export GO111MODULE=on

clean:
	rm -rf **/bin ./vendor Gopkg.lock

#make all micro services
books-create: build
	env GOOS=linux go build -ldflags="-s -w" -o books-create/bin/v1 books-create/v1/main.go

books-find-one: build
	env GOOS=linux go build -ldflags="-s -w" -o books-find-one/bin/v1 books-find-one/v1/main.go

books-find-by-category: build
	env GOOS=linux go build -ldflags="-s -w" -o books-find-by-category/bin/v1 books-find-by-category/v1/main.go

books-delete-one: build
	env GOOS=linux go build -ldflags="-s -w" -o books-delete-one/bin/v1 books-delete-one/v1/main.go

books-queue-one: build
	env GOOS=linux go build -ldflags="-s -w" -o books-queue-one/bin/v1 books-queue-one/v1/main.go